resource "aws_db_subnet_group" "sailpoint" {
  name       = "sp_aurora"
  subnet_ids = ["subnet-0a851a700a744e7b9", "subnet-0b7073e7de4ae0a4e", "subnet-01927a5b8b473b81e"]

  tags = {
    Name = "sailpoint subnet group"
  }
}

resource "aws_rds_cluster" "sailpoint" {
  cluster_identifier        = "aurora-cluster-sailpoint"
  engine                    = "aurora-mysql"
  engine_version            = "5.7.mysql_aurora.2.03.2"
  availability_zones        = ["us-east-1c", "us-east-1d", "us-east-1f"]
  database_name             = "sailpoint"
  master_username           = "sailpoint"
  master_password           = "Password1!"
  backup_retention_period   = 5
  preferred_backup_window   = "02:00-04:00"
  db_subnet_group_name      = "sp_aurora"
  storage_encrypted         = "true"
## skip_final_snapshot      = "true"
  depends_on                = [aws_db_subnet_group.sailpoint]
  final_snapshot_identifier = "sailpoint-aurora-final1"
  vpc_security_group_ids    = [aws_security_group.sailpoint-rds.id]
}

resource "aws_rds_cluster_instance" "sailpoint" {
  count              = 2
  identifier         = "aurora-cluster-demo-${count.index}"
  cluster_identifier = aws_rds_cluster.sailpoint.id
  instance_class     = "db.r5.xlarge"
  engine             = aws_rds_cluster.sailpoint.engine
  engine_version     = aws_rds_cluster.sailpoint.engine_version
  db_subnet_group_name    = "sp_aurora"

}
