resource "aws_iam_role" "cms_backup_role" {
  name = "cms-backup-role"
 
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "dlm.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}
 
resource "aws_iam_role_policy" "cms_backup" {
  name = "cms-backup-policy"
  role = aws_iam_role.cms_backup_role.id
 
  policy = <<EOF
{
   "Version": "2012-10-17",
   "Statement": [
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateSnapshot",
            "ec2:DeleteSnapshot",
            "ec2:DescribeVolumes",
            "ec2:DescribeSnapshots"
         ],
         "Resource": "*"
      },
      {
         "Effect": "Allow",
         "Action": [
            "ec2:CreateTags"
         ],
         "Resource": "arn:aws:ec2:*::snapshot/*"
      }
   ]
}
EOF
}
 
resource "aws_dlm_lifecycle_policy" "cms_backuprole" {
  description        = "DLM lifecycle policy"
  execution_role_arn = aws_iam_role.cms_backup_role.arn
  state              = "ENABLED"
 
  policy_details {
    resource_types = ["VOLUME"]
 
    schedule {
      name = "2 weeks of daily snapshots"
 
      create_rule {
        interval      = 24
        interval_unit = "HOURS"
        times         = ["23:45"]
      }
 
      retain_rule {
        count = 14
      }
 
      tags_to_add = {
        SnapshotCreator = "DLM"
      }
 
      copy_tags = false
    }
 
    target_tags = {
      Snapshot = "true"
    }
  }
}
