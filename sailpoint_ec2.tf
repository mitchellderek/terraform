locals {
###task1###
  name_tag1     = "cor1cspt001"
  az1           = "us-east-1c"
  subnet1       = "subnet-0a851a700a744e7b9"
###task2###
  name_tag2     = "cor1dspt001"
  az2           = "us-east-1d"
  subnet2       = "subnet-0b7073e7de4ae0a4e"
###web1####
  name_tag3     = "cor1cspw001"
  az3           = "us-east-1c"
  subnet3       = "subnet-0a851a700a744e7b9"
####web2####
  name_tag4     = "cor1dspw001"
  az4           = "us-east-1d"
  subnet4       = "subnet-0b7073e7de4ae0a4e"
###universal####
  ebs_size      = "100"
  ami           = "ami-0c41a1c8219749d5f"
  instance_type = "c5.xlarge"
  key           = "uberether-demo-cms"
  profile       = "iam.role.gitlab-runner"
  sg_1          = aws_security_group.sailpoint-ec2.id
  sg_2          = "sg-0f2bd1842a6e6588d"
  userdata      = file("sailpoint_userdata.sh")
}

resource "aws_network_interface" "sp_task1" {
  subnet_id   = local.subnet1
  security_groups = [
    local.sg_1,
    local.sg_2
  ] 
  tags = {
    Name = local.name_tag1
  }
}

resource "aws_ebs_volume" "sp_task1" {
  availability_zone     = local.az1
  size                  = local.ebs_size
  encrypted             = "true"
  tags = {
    Name = "${local.name_tag1}-opt" 
  }
}

resource "aws_instance" "sp_task1" {
  ami           = local.ami 
  instance_type = local.instance_type
  key_name      = local.key
  iam_instance_profile = local.profile
  user_data            = "file(${local.userdata})"
  tags = {
    Name = local.name_tag1
  }

  network_interface {
    network_interface_id = aws_network_interface.sp_task1.id
    device_index         = 0
  }
}

resource "aws_volume_attachment" "sp_task1" {
  device_name = "/dev/sdg"
  volume_id   = aws_ebs_volume.sp_task1.id
  instance_id = aws_instance.sp_task1.id
}

resource "aws_network_interface" "sp_task2" {
  subnet_id   = local.subnet2
  security_groups = [
    local.sg_1,
    local.sg_2
  ]
  tags = {
    Name = local.name_tag2
  }
}

resource "aws_ebs_volume" "sp_task2" {
  availability_zone     = local.az2
  size                  = local.ebs_size
  encrypted             = "true"
  tags = {
    Name = "${local.name_tag2}-opt"
  }
}

resource "aws_instance" "sp_task2" {
  ami           = local.ami
  instance_type = local.instance_type
  key_name      = local.key
  iam_instance_profile = local.profile
  user_data            = "file(${local.userdata})"
  tags = {
    Name = local.name_tag2
  }

  network_interface {
    network_interface_id = aws_network_interface.sp_task2.id
    device_index         = 0
  }
}

resource "aws_volume_attachment" "sp_task2" {
  device_name = "/dev/sdg"
  volume_id   = aws_ebs_volume.sp_task2.id
  instance_id = aws_instance.sp_task2.id
}

resource "aws_network_interface" "sp_task3" {
  subnet_id   = local.subnet3
  security_groups = [
    local.sg_1,
    local.sg_2
  ]
  tags = {
    Name = local.name_tag3
  }
}

resource "aws_ebs_volume" "sp_task3" {
  availability_zone     = local.az3
  size                  = local.ebs_size
  encrypted             = "true"
  tags = {
    Name = "${local.name_tag3}-opt"
  }
}

resource "aws_instance" "sp_task3" {
  ami           = local.ami
  instance_type = local.instance_type
  key_name      = local.key
  iam_instance_profile = local.profile
  user_data            = "file(${local.userdata})"
  tags = {
    Name = local.name_tag3
  }

  network_interface {
    network_interface_id = aws_network_interface.sp_task3.id
    device_index         = 0
  }
}

resource "aws_volume_attachment" "sp_task3" {
  device_name = "/dev/sdg"
  volume_id   = aws_ebs_volume.sp_task3.id
  instance_id = aws_instance.sp_task3.id
}

resource "aws_network_interface" "sp_task4" {
  subnet_id   = local.subnet4
  security_groups = [
    local.sg_1,
    local.sg_2
  ]
  tags = {
    Name = local.name_tag4
  }
}

resource "aws_ebs_volume" "sp_task4" {
  availability_zone     = local.az4
  size                  = local.ebs_size
  encrypted             = "true"
  tags = {
    Name = "${local.name_tag4}-opt"
  }
}

resource "aws_instance" "sp_task4" {
  ami           = local.ami
  instance_type = local.instance_type
  key_name      = local.key
  iam_instance_profile = local.profile
  user_data            = "file(${local.userdata})"
  tags = {
    Name = local.name_tag4
  }

  network_interface {
    network_interface_id = aws_network_interface.sp_task4.id
    device_index         = 0
  }
}

resource "aws_volume_attachment" "sp_task4" {
  device_name = "/dev/sdg"
  volume_id   = aws_ebs_volume.sp_task4.id
  instance_id = aws_instance.sp_task4.id
}
