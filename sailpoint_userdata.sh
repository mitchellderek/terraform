#! /bin/bash
sudo export http_proxy=https://192.168.0.7:3128
sudo export https_proxy=https://192.168.0.7:3128
sudo export NO_PROXY=169.254.169.254
sudo echo "proxy=https://192.168.0.7:3128" >> /etc/yum.conf
sudo yum install -y bind-utils net-tools mysql unzip system-logos wget

sudo cd /opt
sudo curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"
sudo unzip awscli-bundle.zip
sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws

sudo systemctl stop iptables
sudo systemctl disable iptables

sudo aws s3 cp s3://uberether-demo-software/rhel/tag_volume.sh /root/
sudo aws s3 cp s3://uberether-demo-software/rhel/cms_mount_opt.sh /root/
sudo chmod +x /root/tag_volume.sh
sudo chmod +x /root/cms_mount_opt.sh
sudo bash /root/tag_volume.sh
sudo bash /root/cms_mount_opt.sh
sudo rm -f /root/tag_volume.sh
sudo rm -f /root/cms_mount_opt.sh
