resource "aws_security_group" "sailpoint-ec2" {
  name        = "sailpoint-ec2"
  description = "Allow inbound traffic to sailpoint ec2 instances"
  vpc_id      = "vpc-02548c6ea15a8a48b"

  ingress {
    description = "ssh from VPC"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "10.251.0.0/19",
      "192.168.0.0/24"
    ]
  }
  ingress {
    description = "https from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [  
      "10.251.0.0/19",
      "192.168.0.0/24"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sailpoint-ec2"
  }
}
resource "aws_security_group" "sailpoint-rds" {
  name        = "sailpoint-rds"
  description = "Allow TCP from Sailpoint Instances"
  vpc_id      = "vpc-02548c6ea15a8a48b"
  depends_on  = [aws_security_group.sailpoint-ec2]

  ingress {
    description = "TCP from Sailpoint Instances"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    security_groups = [aws_security_group.sailpoint-ec2.id] 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "sailpoint-rds"
  }
}
